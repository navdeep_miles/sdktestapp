package connectiq.miles.sdktestapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

//import connectiq.miles.reporting.MilesReportingSdk;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import connectiq.miles.reporting.MilesBridge;
import connectiq.miles.reporting.MilesServiceSdk;
import connectiq.miles.reporting.callbacks.MilesCallback;
//import connectiq.miles.reporting.onboardingstate.OnboardingState;
import connectiq.miles.reporting.result.MilesResult;
import connectiq.miles.reporting.trips.Analytics;
import connectiq.miles.reporting.trips.MilebankData;
import connectiq.miles.reporting.trips.MultiplierBreakdownData;
import connectiq.miles.reporting.trips.SdkTripNetworkRequestHelper;
import connectiq.miles.reporting.trips.TransactionsData;
import connectiq.miles.reporting.trips.TripMonthData;
import connectiq.miles.reporting.trips.b;
import connectiq.miles.reporting.user.MilesSDKException;
import connectiq.miles.reporting.user.UserAuthentication;

public class MainActivity extends AppCompatActivity {

    //According to new Android guidelines, we need to have a foreground location permission before asking for a background permission
    //hence they should be asked sequentially. For sample scenario, first we will allow foreground permission and then click on background button.

    private static final String[] PERMISSIONS_ARRAY_FOREGROUND = new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION};
    private static final String[] PERMISSIONS_ARRAY_BACKGROUND = Build.VERSION.SDK_INT > Build.VERSION_CODES.Q ? new String[]{Manifest.permission.ACCESS_BACKGROUND_LOCATION} : new String[]{};

    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        TextView sample = ActivityCompat.requireViewById(this, R.id.sample);
        Button foregroundButton = ActivityCompat.requireViewById(this, R.id.foreground);
        Button backgroundButton = ActivityCompat.requireViewById(this, R.id.background);

        foregroundButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ActivityCompat.requestPermissions(MainActivity.this, PERMISSIONS_ARRAY_FOREGROUND, 44);
                //Needs import, request code can be custom(anything as required)
//                MilesReportingSdk.getInstance().requestPermissions(MainActivity.this, 11);
            }
        });

        backgroundButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ActivityCompat.requestPermissions(MainActivity.this, PERMISSIONS_ARRAY_BACKGROUND, 45);
                //Needs import, request code can be custom(anything as required)
//                MilesReportingSdk.getInstance().requestPermissions(MainActivity.this, 11);
            }
        });
        // We would initialize SDK here for the same app here, like the code below
//        MilesServiceSdk.initialize(getApplication(), partnerKey, partnerSecret);
//

        //Method calls should be subsequently called after initialization
    }
}